package com.example.charitym;

public class actes {
    private  String reference;
    private  String status;

    public actes(String reference, String status) {
        this.reference = reference;
        this.status = status;
    }
    public actes() {

    }
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
