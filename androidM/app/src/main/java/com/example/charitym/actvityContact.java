package com.example.charitym;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class actvityContact extends AppCompatActivity {
    Button b1;
    private  DatabaseHelper db;
    EditText t1,t2,t3;
    Boolean  EditTextEmptyHold;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact);
        db= new DatabaseHelper(this);
        t1=findViewById(R.id.adr_donateur);
        t2=findViewById(R.id.adr_asso);
        t3=findViewById(R.id.statut);
        b1=findViewById(R.id.send);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckEditTextStatus();
                InsertDataIntoSQLiteDatabase();
                EmptyEditTextAfterDataInsert();
            }
        });}
    public void CheckEditTextStatus(){

        String adr_do = t1.getText().toString();
        String adr_ass = t2.getText().toString();
        String status = t3.getText().toString();
        if(TextUtils.isEmpty(adr_ass) || TextUtils.isEmpty(adr_do) ||TextUtils.isEmpty(status)){

            EditTextEmptyHold = false ;

        }
        else {

            EditTextEmptyHold = true ;
        }
    }
    public void InsertDataIntoSQLiteDatabase(){

        if(EditTextEmptyHold == true)
        {
            String adrD= t1.getText().toString();
            String adrA = t2.getText().toString();
            String status = t3.getText().toString();
            contact s =new contact(adrA,adrD,status);
            db.addContact(s);
            finish();
            Toast.makeText(actvityContact.this,"Data Inserted Successfully", Toast.LENGTH_LONG).show();
            Intent i=new Intent(actvityContact.this,MainActivity.class);
            startActivity(i);
        }
        else {

            Toast.makeText(actvityContact.this,"Please Fill All The Required Fields.", Toast.LENGTH_LONG).show();

        }

    }
    public void EmptyEditTextAfterDataInsert(){

        t1.getText().clear();
        t2.getText().clear();

    }
}

