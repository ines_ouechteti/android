package com.example.charitym;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class login extends AppCompatActivity {

    EditText t1,t2;
    Button b1 ;
    TextView a;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        t1=findViewById(R.id.edit_Email);
        t2=findViewById(R.id.edit_Password);
        b1=findViewById(R.id.btn_Connexion);
        a=findViewById(R.id.reg);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String log = t1.getText().toString();
                String pw = t2.getText().toString();
                if (log.equals("") || pw.equals(""))
                    Toast.makeText(login.this, "champs Vide", Toast.LENGTH_SHORT).show();
                else {
                    SharedPreferences sp=getSharedPreferences("MyInfo",MODE_PRIVATE);
                    String login=sp.getString("l","");
                    String password=sp.getString("p","");

                        Intent i = new Intent(login.this, MainActivity.class);

                        startActivity(i);
                    }
                }

        });
        a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(login.this,registre.class);
                startActivity(i);
            }
        });


    }
}
