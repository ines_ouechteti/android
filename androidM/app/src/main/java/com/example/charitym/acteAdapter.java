package com.example.charitym;
import android.app.Activity;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;


public class acteAdapter extends ArrayAdapter<actes> {
    Activity context;
    ArrayList<actes> listacte;
    DatabaseHelper db;

    public acteAdapter(Activity context, ArrayList<actes> listacte) {
        super(context,R.layout.actes_item,listacte );
        this.context = context;
        this.listacte = listacte;
    }
    @NonNull
    @Override
    public View getView(int position, @NonNull View convertView, @NonNull ViewGroup parent){

        View view= LayoutInflater.from(context).inflate(R.layout.actes_item,null,false);
        TextView t2=view.findViewById(R.id.reference);
        TextView t3=view.findViewById(R.id.statut);
        ImageView im1=view.findViewById(R.id.edit_student);
        ImageView im2=view.findViewById(R.id.delete_student);

        t2.setText(String.valueOf(listacte.get(position).getReference()));
        t3.setText(listacte.get(position).getStatus());
        actes s=listacte.get(position);
        im1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater=LayoutInflater.from(context);
                View subView=inflater.inflate(R.layout.activity_update,null,false);

                EditText p=subView.findViewById(R.id.statut);

                p.setText(s.getStatus());
                AlertDialog.Builder a=new AlertDialog.Builder(context);
                a.setTitle("");
                a.setView(subView);
                a.create() ;
                a.setPositiveButton("edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String status=p.getText().toString();

                        db=new DatabaseHelper(context);
                        db.updateActe(new actes(s.getReference(),status));
                        context.finish();
                        context.startActivity(context.getIntent());



                    }
                });
                a.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(context,"task cancel",Toast.LENGTH_LONG).show();
                    }
                });
                a.show();
            }
        });



        im2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db=new DatabaseHelper(context);
                db.removeActe(s.getReference());
                context.finish();
                context.startActivity(context.getIntent());
            }
        });


        return view;

    }



}

