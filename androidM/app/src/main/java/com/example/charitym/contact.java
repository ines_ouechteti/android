package com.example.charitym;

public class contact {
    private  String emaildonateur;
    private  String emailassociation;
    private  String status;

    public contact(String emaildonateur, String emailassociation, String status) {
        this.emaildonateur = emaildonateur;
        this.emailassociation = emailassociation;
        this.status = status;
    }
    public contact() {}

    public String getEmaildonateur() {
        return emaildonateur;
    }

    public void setEmaildonateur(String emaildonateur) {
        this.emaildonateur = emaildonateur;
    }

    public String getEmailassociation() {
        return emailassociation;
    }

    public void setEmailassociation(String emailassociation) {
        this.emailassociation = emailassociation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
