package com.example.charitym;

import android.database.sqlite.SQLiteOpenHelper;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;

import android.database.Cursor;


import androidx.annotation.Nullable;

import java.util.ArrayList;
public  class DatabaseHelper extends SQLiteOpenHelper {
    public static  final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "charity_db";

    public static final String TABLE_ACTES = "actes";
    public static final String COL_REFERENCE = "reference";
    public static final String COL_STATUS = "status";

    public static final String TABLE_UTILISTAEUR = "utilisateur";
    public static final String COL_TYPE = "type";
    public static final String COL_EMAIL = "email";
    public static final String COL_PASSWORD = "password";
    public static final String COL_NOM = "nom";
    public static final String COL_PRENOM = "prenom";

    public static final String TABLE_CONTACT = "contact";
    public static final String COL_EMAILDONATEUR = "emaildonateur";
    public static final String COL_EMAILASSOCIATION = "emailassociation";
    public static final String COL_STATUSCONTACT = "statuscontact";



    public static final String CREATE_ACTES_TABLE = "CREATE TABLE " + TABLE_ACTES + "("
            + COL_REFERENCE + " TEXT PRIMARY KEY, "
            + COL_STATUS + " TEXT NOT NULL) ";

    public static final String CREATE_UTILISATEUR_TABLE = "CREATE TABLE " + TABLE_UTILISTAEUR + "("
            + COL_TYPE + " TEXT PRIMARY KEY, "
            + COL_EMAIL + " TEXT NOT NULL, "
            + COL_PASSWORD + " TEXT NOT NULL, "
            + COL_NOM + " TEXT NOT NULL, "
            + COL_PRENOM + " TEXT NOT NULL) ";

    public static final String CREATE_CONTACT_TABLE = "CREATE TABLE " + TABLE_CONTACT + "("
            + COL_EMAILDONATEUR + " TEXT NOT NULL, "
            + COL_EMAILASSOCIATION + " TEXT NOT NULL, "
            + COL_STATUSCONTACT + " TEXT PRIMARY KEY) ";

    private SQLiteDatabase db;


    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ACTES_TABLE);
        db.execSQL(CREATE_UTILISATEUR_TABLE);
        db.execSQL(CREATE_CONTACT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF exists "+TABLE_ACTES);
        db.execSQL("DROP TABLE IF exists "+TABLE_UTILISTAEUR);
        db.execSQL("DROP TABLE IF exists "+TABLE_CONTACT);
        onCreate(db);
    }
    public SQLiteDatabase open(){

        db=this.getWritableDatabase();
        return db;
    }

    public void addactes(actes s ){
        open();
        ContentValues v =new ContentValues();
        v.put(COL_REFERENCE,s.getReference());
        v.put(COL_STATUS,s.getStatus());
        db.insert(TABLE_ACTES,null,v);
    }
    public void addutilisateur(Utilisateur u ){
        open();
        ContentValues v =new ContentValues();
        v.put(COL_TYPE,u.getType());
        v.put(COL_EMAIL,u.getEmail());
        v.put(COL_PASSWORD,u.getPassword());
        v.put(COL_NOM,u.getNom());
        v.put(COL_PRENOM,u.getPrenom());
        db.insert(TABLE_UTILISTAEUR,null,v);
    }
    public void addContact(contact c ){
        open();
        ContentValues v =new ContentValues();
        v.put(COL_EMAILASSOCIATION,c.getEmailassociation());
        v.put(COL_EMAILDONATEUR,c.getEmaildonateur());
        v.put(COL_STATUSCONTACT,c.getStatus());
        db.insert(TABLE_CONTACT,null,v);
    }
    public void updateActe(actes a) {
        open();
        ContentValues values = new ContentValues();

        values.put(COL_STATUS,a.getStatus());
        db.update(TABLE_ACTES, values, COL_REFERENCE + "=?", new String[]{String.valueOf(a.getReference())});

    }

    public void removeActe(String id) {
        open();
        db.delete(TABLE_ACTES,   COL_REFERENCE +"=?", new String[]{id});
    }

    public void updateProfil(Utilisateur U, int cin) {
        open();
        ContentValues values = new ContentValues();
        values.put(COL_NOM, U.getNom());
        values.put(COL_PASSWORD, U.getPassword());
        values.put(COL_PRENOM, U.getPrenom());
        values.put(COL_EMAIL,U.getEmail());
        db.update(TABLE_ACTES, values, COL_REFERENCE + "=?", new String[]{String.valueOf((cin))});

    }

    //login
    public Boolean checkEmail(String email){
        db=this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from Utiisateur where email = ?",new String[] {email});
        if (cursor.getCount()>0)
            return true;
        else
            return false;
    }

    public Boolean checkEmailPassword(String email, String password){
        db=this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from Utilisateur where email = ? and password = ?",new String[] {email,password});
        if (cursor.getCount()>0)
            return true;
        else
            return false;
    }

    //AFFICHAGE
    public ArrayList<actes> getAllactes() {
        db = this.getReadableDatabase();
        ArrayList<actes> list = new ArrayList<actes>();
        Cursor c = db.query(TABLE_ACTES, new String[]{COL_REFERENCE, COL_STATUS}, null, null, null, null, null);
        c.moveToFirst();
        do {
            actes s = new actes(c.getString(0), c.getString(1));
            list.add(s);
        } while (c.moveToNext());
        return list;
    }
    //COUNT
    public int getACTECount() {
        db =this.getReadableDatabase();
        Cursor c = db.query(TABLE_ACTES, new String[]{COL_REFERENCE, COL_STATUS},null,null,null,null,null);
        return c.getCount();

    }


}
