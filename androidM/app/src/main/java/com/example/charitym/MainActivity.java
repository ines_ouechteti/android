package com.example.charitym;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView l;
    private DatabaseHelper db;
    private acteAdapter adapter;
    private ArrayList<actes> listacte;
    private FloatingActionButton btn_add;
    private ImageView edit;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.mymenu,menu);
        return true ;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.lagout:
                Intent i = new Intent(MainActivity.this,login.class);
                startActivity(i);
                return true ;
            case R.id.about:
                Intent a = new Intent(MainActivity.this,about.class);
                startActivity(a);
                return true ;
            case R.id.profil:
                Intent f = new Intent(MainActivity.this,profil.class);
                startActivity(f);
                return true ;
            case R.id.contact:
                Intent c = new Intent(MainActivity.this,actvityContact.class);
                startActivity(c);
                return true ;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        l=findViewById(R.id.list_view);
        db=new DatabaseHelper(this);
        int nbStudent=db.getACTECount();
        if(nbStudent==0)
            Toast.makeText(this, "Empty database", Toast.LENGTH_SHORT).show();
        else
        {
            listacte=db.getAllactes();
            adapter=new acteAdapter(this,listacte);
            l.setAdapter(adapter);
        }
        btn_add=findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater=LayoutInflater.from(MainActivity.this);
                View subView=inflater.inflate(R.layout.activity_add,null,false);
                EditText n=subView.findViewById(R.id.reference);
                EditText p=subView.findViewById(R.id.statut);
                AlertDialog.Builder a=new AlertDialog.Builder(MainActivity.this);
                a.setTitle("");
                a.setView(subView);
                a.create() ;
                a.setPositiveButton("add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String ref=n.getText().toString();
                        String statut=p.getText().toString();
                        actes s=new actes(ref,statut);
                        db.addactes(s);
                        finish();
                        startActivity(getIntent());

                    }
                });
                a.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this,"task cancel",Toast.LENGTH_LONG).show();
                    }
                });
                a.show();
            }

        });


    }
}
