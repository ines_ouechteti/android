package com.example.charitym;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

public class registre extends AppCompatActivity {
    private DatabaseHelper db;
    EditText t1,t2,t3,t4,t5,t6,t7;
    Button b1 , b2 , b3 ;
    RadioButton a1 , a2 ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registre);

         //attribut registre
        t3 = findViewById(R.id.name);
        t4 = findViewById(R.id.prenom);
        t5 = findViewById(R.id.email);
        t6 = findViewById(R.id.passe);
        t7 = findViewById(R.id.conf_motp);
        a1 = findViewById(R.id.b_agent);
        a1 = findViewById(R.id.b_agent);
        b2 = findViewById(R.id.creer);
        b3 = findViewById(R.id.annuller);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences p=getSharedPreferences("MyInfo",MODE_PRIVATE);
                SharedPreferences.Editor editor=p.edit();

                String nom=t3.getText().toString();
                editor.putString("n",nom);

                String logiin=t4.getText().toString();
                editor.putString("e",logiin);

                String Email=t5.getText().toString();
                editor.putString("l",Email);

                String pass=t6.getText().toString();
                editor.putString("p",pass);

                String agent=a1.getText().toString();
                editor.putString("a",agent);

                String pass1=t7.getText().toString();
                if (pass1.equals(pass))
                {
                    editor.commit();
                    Intent i=new Intent(registre.this,login.class);
                    Toast.makeText(registre.this,"add count succes",Toast.LENGTH_SHORT).show();
                    startActivity(i);
                }else
                    Toast.makeText(registre.this,"Verifier Votre mot de pass",Toast.LENGTH_SHORT).show();
            }
        });


        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(registre.this,login.class);
                startActivity(i);
            }
        });


    }
}
