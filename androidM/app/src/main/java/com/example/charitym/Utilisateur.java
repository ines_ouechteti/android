package com.example.charitym;

public class Utilisateur {
    private  String type;
    private  String email;
    private  String password;
    private  String nom;
    private  String prenom;

    public Utilisateur(String type, String email, String password, String nom, String prenom) {
        this.type = type;
        this.email = email;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
    }
    public Utilisateur(String name, String prenom, String pass, String email) {}
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
